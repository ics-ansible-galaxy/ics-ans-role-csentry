import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_csentry_containers(host):
    with host.sudo():
        cmd = host.command("docker ps")
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == [
        "csentry_elasticsearch",
        "csentry_postgres",
        "csentry_redis",
        "csentry_web",
        "csentry_worker_0",
        "csentry_worker_1",
        "traefik_proxy",
    ]


def test_csentry_index(host):
    # This tests that traefik forwards traffic to the csentry web server
    # and that we can access the csentry login page
    cmd = host.command(
        "curl -H Host:ics-ans-role-csentry-default -k -L https://localhost"
    )
    assert '<meta name="description" content="CSEntry">' in cmd.stdout
    assert '<title>Login</title>' in cmd.stdout


def test_root_crontab(host):
    with host.sudo():
        crontab = host.check_output("crontab -l")
    # Note that the crontab could include other entries
    assert r"""#Ansible: Run flask maintenance every night
17 2 * * * /usr/bin/docker exec csentry_web flask maintenance > /tmp/flask_maintenance.log 2>&1
#Ansible: Remove old csentry static files
17 0 * * * find /etc/csentry/files -type f -mtime +7 -exec rm -f {} \;
#Ansible: Run database dump
17 1 * * * /usr/local/sbin/dump-db > /tmp/dump-db.log 2>&1""" in crontab


def test_vm_max_map_count(host):
    with host.sudo():
        assert host.sysctl("vm.max_map_count") == 262144


def test_elasticsearch_container_limit(host):
    with host.sudo():
        for limit, result in [
            ("-Hn", "65536"),
            ("-Sn", "65536"),
            ("-Hu", "4096"),
            ("-Su", "4096"),
            ("-Hl", "unlimited"),
            ("-Sl", "unlimited"),
        ]:
            assert (
                host.check_output(
                    "docker exec csentry_elasticsearch bash -c 'ulimit {}'".format(
                        limit
                    )
                ) == result
            )
