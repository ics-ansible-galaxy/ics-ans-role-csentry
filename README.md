ics-ans-role-csentry
====================

Ansible role to install the CSEntry web application.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
csentry_processes: 4
csentry_workers: 2
csentry_settings: "{{ playbook_dir }}/config/settings.cfg"
csentry_network: csentry-network
csentry_redis_tag: 4.0
csentry_postgres_tag: 10
csentry_postgres_user: ics
csentry_postgres_password: ics
csentry_postgres_db: csentry_db
csentry_elasticsearch_tag: 6.4.2
csentry_image: registry.esss.lu.se/ics-infrastructure/csentry
csentry_tag: latest
csentry_hostname: "{{ ansible_fqdn }}"
csentry_uwsgi_buffer_size: 16384
csentry_tower_host: awx.mydomain.org
csentry_tower_oauth_token: token
csentry_tower_verify_ssl: "true"
csentry_enable_threads: "true"
csentry_secret_key: secret
csentry_ldap_bind_user_dn: ldapuser
csentry_ldap_bind_user_password: password
csentry_sentry_dsn: ""
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-csentry
```

License
-------

BSD 2-clause
